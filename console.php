<?php
require_once 'Finder.php';

use Core\Finder;

$finder = new Finder;
$finder->config['console'] = true;
$finder->start();
