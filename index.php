<?php
require_once 'Finder.php';

use Core\Finder;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>PHP BOM Finder</title>
	<link rel="stylesheet" href="index.css" type="text/css">
</head>
<body>
	<div id="container">
		<header>
			<h1>BOMFinder</h1>
		</header>
		<section>
			<h2>Log</h2>
			<?php
				$finder = new Finder;
				$finder->start();
			?>
		</section>
		<footer>
			&copy; 2013 - Simon Péter - <a href="http://simonnetwork.hu" target="_blank">Simon Network</a>
		</footer>
	</div>
</body>
</html>