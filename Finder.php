<?php
namespace Core;

class Finder {
	public $config = array(
		'replace_bom'	=> true, 								//ha true, akkor lecseréli a bom értékét
		'folder'		=> '/home/petersimon/web/bom_checker/',	//scannelni kívánt folder
		'extensions'	=> array('php', 'txt', 'html'),						//kiterjesztése
		'console'		=> false
	);
	private $console;
	public $counter = array(
		'all'		=> 0,
		'no_boom'	=> 0,
		'boom'		=> 0,
		'replaced'	=> 0,
		'unchecked'	=> 0
	);

	/**
	 * Start search
	 */
	function start() {
		$this->checkDir();
		$this->endScript();
	}
	
	/**
	 * Directory scan
	 */
	function checkDir() {
		$source_dir			= $this->fixDir();
		$source_dir_handler = opendir($source_dir);

		while($file = readdir($source_dir_handler)) {
			// Skip ".", ".." and hidden fields (Unix).
			if(mb_substr($file, 0, 1) == '.')
				continue;

			$source_file_path = $source_dir.$file ;

			if(is_dir($source_file_path)){
				$counter += $this->checkDir($source_file_path);
			}

			if($this->getFileExtension($source_file_path)) {
				if (!is_file($source_file_path) || !$this->checkBom($source_file_path)) {
					$this->counter['no_bom']++;
					$this->log('No BOM detected: '.$source_file_path);
				} else {
					$this->counter['bom']++;
					$this->log('BOM Detected: '.$source_file_path);
					
					if($this->config['replace_bom']) {
						$this->replaceBom($source_file_path);
						$this->counter['replaced']++;
					}
				}
			} else {
				$this->counter['unchecked']++;
				$this->log('Unchecked: '.$source_file_path);
			}
			
			$this->counter['all']++;
		}
	}

	/**
	 *
	 */
	private function fixDir() {
		$dir_path = str_replace('\\', '/', $this->config['folder']);

		if(mb_substr($dir_path, -1, 1 ) != '/')
			$dir_path .= '/';

		return $dir_path;
	}

	/**
	 *
	 */
	private function getFileExtension($file_path) {
		$file_extension = pathinfo($file_path);
		return in_array($file_extension['extension'], $this->config['extensions']) ? true : false;
	}
	
	/**
	 * 
	 */
	private function replaceBom($file_path) {
		$data = file_get_contents($file_path);
		if(@file_put_contents($file_path, mb_substr($data, 3)))
			$this->log('BOM removed');
		else
			$this->log('Permission denied: '.$file_path);
	}

	/**
	 * Check UTF-8 BOM
	 */
	private function checkBom($file_path) {
		$data = file_get_contents($file_path);
		return (mb_substr($data, 0, 3) == "\xEF\xBB\xBF");
	}
	
	/**
	 * Log
	 */
	private function log($content, $level = 'NOTICE') {
		echo $content."\r\n";
		if(!$this->config['console'])
			echo '<br>';
		error_log($level.' - '.$content.' - '.date('Y-m-d H:i:s'), 3, 'run.log');
	}
	
	/**
	 * Get memory usage
	 */
	function memoryUsage() {
		return array(
			'memory'			=> memory_get_usage(),
			'memory-real'		=> memory_get_usage(true),
			'memory-peak'		=> memory_get_peak_usage(true),
			'memory-peak-real'	=> memory_get_peak_usage(true)
		);
	}
	
	/**
	 * XSS defense
	 */
	function h($content) {
		return htmlentities($content, ENT_QUOTES);
	}
	
	/**
	 *
	 */
	private function endScript() {
		$this->log(print_r($this->counter, true));
		$this->log(print_r($this->memoryUsage(), true));
	}
}
